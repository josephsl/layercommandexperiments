# Layered Commands Experiment
# An experimental global plugin.

import tones
import globalPluginHandler
#import scriptHandler
from functools import wraps
import ui
import addonHandler

# Below toggle code came from Tyler Spivey's code, with enhancements by Joseph Lee.

def finally_(func, final):
	"""Calls final after func, even if it fails."""
	def wrap(f):
		@wraps(f)
		def new(*args, **kwargs):
			try:
				func(*args, **kwargs)
			finally:
				final()
		return new
	return wrap(final)

class GlobalPlugin(globalPluginHandler.GlobalPlugin):


	def __init__(self, *args, **kwargs):
		super(GlobalPlugin, self).__init__(*args, **kwargs)
		self.toggling = False

	def getScript(self, gesture):
		if not self.toggling:
			return globalPluginHandler.GlobalPlugin.getScript(self, gesture)
		script = globalPluginHandler.GlobalPlugin.getScript(self, gesture)
		if not script:
			script = finally_(self.script_error, self.finish)
		return finally_(script, self.finish)

	def finish(self):
		self.toggling = False
		self.clearGestureBindings()
		self.bindGestures(self.__gestures)

	def script_error(self, gesture):
		tones.beep(120, 100)


	def script_toggle(self, gesture):
		# A run-time binding will occur from which we can perform various layered translation commands.
		# First, check if a second press of the script was done.
		if self.toggling:
			self.script_error(gesture)
			return
		self.bindGestures(self.__testGestures)
		self.toggling = True
		tones.beep(512, 10)
	script_toggle.__doc__=_("Instant Translate layer commands. Press C to translate clipboard text, t to translate selction or s to swap languages.")

	def script_hello(self, gesture):
		ui.message("hello")
		self.finish()

	__testGestures={
		"kb:a":"hello"
	}

	__gestures={
		"kb:nvda+shift+q":"toggle"
	}