# Layer Commands #

* Authors: Tyler Spivey, Joseph Lee

This document explains how layer commands work in NVDA.

Note that this is a temporary solution from the community; a ticket has been filed to allow layered (i.e. prefixed) commands to ber integrated into NVDA core.

# How to use this package #

If you wish to use layer commands in your add-on, follow these steps:

* Extract this package, then look for addon/GlobalPlugins/LayerCommandExperiment.py (change this later).
* Copy the contents of this file (not the file itself but what's in it) into the module where you wish to use the layer commands. For example, if you want to use layer commands in your app module, copy the file contents to appModules/yourAppModule.py.
* (future) Write your add-on header such as needed imports. Then after the list of imports, place the functool part of the package below it.
* After you declare either an AppModule or a GlobalPlugin class, paste the driver part into the file.

# Layer command code #

In your module, you need to have the following:

* The actual layer toggle function.
* At least two gesture dictionaries - one to hold the actual module gestures (including the layer toggle command) and others for each layer.

For an example layer command module, consult the layer commands file itself.

